var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function() {
  var schema = mongoose.Schema({
    texto: {
      type: String,
      required: true
    },
    likes: {
      type: String,
      required: true
    },
    uid: {
      type: Schema.ObjectId,
      required: true
    }
  });
  return mongoose.model('Post', schema);
}();
