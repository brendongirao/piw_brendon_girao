var Usuario = require('../models/usuario.js');
var Post = require('../models/post.js');
var bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

// var posts = require('./posts.js');
// lista de usuarios
// var usuarios = [{_id:1, nome:"Pedro", email:"pedro@gmail.com", senha:"pedro123"},
//                 {_id:2, nome:"Alice", email:"alice@gmail.com", senha:"alice123"},
//                 {_id:3, nome:"Maria", email:"maria@gmail.com", senha:"maria123"},
//                 {_id:4, nome:"João", email:"joao@gmail.com", senha:"joao123"}];

// module.exports.usuarios = usuarios;



//RECUPERAR TODOS OS USUARIOS
module.exports.listaUsuarios = function(req, res){
  // res.json(usuarios);
  let promise = Usuario.find().exec();
  promise.then(
    function(usuarios){
      res.json(usuarios)
    },
    function(erro){
      res.status(500).end();
    }
  );
};

//RECUPERAR UM USUARIO
module.exports.obterUsuario = function(req, res){
  // var id = req.params.id;
  // var usuarioSelecionado = usuarios.find(function(usuario){
  //                                         return usuario._id == id;
  //                                       })
  // if(usuarioSelecionado){
  //   res.json(usuarios[id-1]);
  // }else{
  //   res.status(404).send("Usuário não encontrado")
  // }
  var id = req.params.id;
  let promise = Usuario.findById(id).exec();

  promise.then(
    function(usuario){
      res.json(usuario)
    },
    function(erro){
      res.status(500).end()
    }
  )
};

//INSERIR UM USUARIO
module.exports.inserirUsuario = function(req, res){
  // usuarios.push(req.body);
  // res.status(200).send(req.body);
  let usuario = new Usuario({
    nome: req.body.nome,
    email: req.body.email,
    senha: bcrypt.hashSync(req.body.senha, 10)
  })
  let token = jwt.sign({usuario: usuario}, 'secret');
  let promise = Usuario.create(usuario);
  promise.then(
    function(usuario){
      res.status(201).json(token);
    },
    function(erro){
      res.status(500).json(erro)
    }
  );
};

//EDITAR UM USUARIO
module.exports.editarUsuario = function(req, res){
  // var id = req.params.id;
  // var usuarioSelecionado = usuarios.find(function(usuario){
  //                                         return usuario._id == id;
  //                                       })
  // if(usuarioSelecionado){
  //   for (var i = 0; i < usuarios.length; i++) {
  //     if (usuarioSelecionado==usuarios[i]) {
  //       usuarios[i] = req.body;
  //       res.status(200).send(req.body);
  //     }
  //   }
  // }else{
  //   res.status(404).send("Usuário não encontrado")
  // }
  let id = req.params.id;
  let promise = Usuario.update(Usuario.findById(id), req.body);
  promise.then(
    function(usuario){
      res.status(200).json(usuario);
    },
    function(erro){
      res.status(500).json(erro);
    }
  )
};

//REMOVER UM USUARIO
module.exports.removerUsuario = function(req, res){
  // var id = req.params.id;
  // var usuarioSelecionado = usuarios.find(function(usuario){
  //                                         return usuario._id == id;
  //                                       })
  // if (usuarioSelecionado) {
  //   for (var i = 0; i < usuarios.length; i++) {
  //     if (usuarioSelecionado==usuarios[i]) {
  //       usuarios.splice(i,1);
  //       res.status(200).send(req.body);
  //     }
  //   }
  // }else{
  //   res.status(404).send("Usuário não encontrado")
  // }
  let id = req.params.id;
  let promise = Usuario.findByIdAndRemove(id);
  promise.then(
    function(usuario){
      res.status(200).json(usuario);
    },
    function(erro){
      res.status(500).json(erro);
    }
  )
}

//OBTER POSTS DE UM USUARIO
module.exports.obterPostsDeUsuario = function(req, res){
  // var idUsuario = req.params.id;
  // var idUsuario = Usuario.find({'_id':req.params.id}).exec();
  // var postsUsuario = [];
  // for (var i = 0; i < posts.posts.length; i++) {
  //   if (posts.posts[i].uid==idUsuario) {
  //     postsUsuario.push(posts.posts[i]);
  //   }
  // }
  // if (postsUsuario.length==0) {
  //   res.status(404).send("Usuário sem posts")
  // }
  // res.json(postsUsuario);
  // console.log(idUsuario);
  // let contar = Usuario.count({'nome':"Pedrão"}).exec();
  let promise = Post.find({'uid':req.params.id}).exec();
  promise.then(
    function(posts){
      console.log('entrou');
      res.status(200).json(posts);
    },
    function(erro){
      res.status(500).json(erro);
    }
  )
}
