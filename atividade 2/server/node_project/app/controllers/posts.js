var Post = require('../models/post.js');
var Usuario = require('../models/usuario.js');
let jwt = require('jsonwebtoken');

// var usuarios = require('./usuarios.js');
//lista de posts
// var posts = [{_id:1, texto:"Texto1", likes:"1", uid:"1"},
//             {_id:2, texto:"Texto2", likes:"2", uid:"1"},
//             {_id:3, texto:"Texto3", likes:"3", uid:"2"}];

// module.exports.posts = posts;

//RECUPERAR TODOS OS POSTS
module.exports.listarPosts = function(req, res){
  // res.json(posts);
  let promise = Post.find().exec();
  promise.then(
    function(posts){
      res.json(posts)
    },
    function(erro){
      res.status(500).end();
    }
  );
};

//RECUPERAR UM POST
module.exports.obterPost = function(req, res){
  // var id = req.params.id;
  // var postSelecionado = posts.find(function(post){
  //                                         return post._id == id;
  //                                       })
  // if(postSelecionado){
  //   res.json(posts[id-1]);
  // }else{
  //   res.status(404).send("Post não encontrado")
  // }
  var id = req.params.id;
  let promise = Post.findById(id).exec();

  promise.then(
    function(post){
      res.json(post)
    },
    function(erro){
      res.status(500).end()
    }
  )
};

//INSERIR UM POST
module.exports.inserirPost = function(req, res){
  // posts.push(req.body);
  // res.status(200).send(req.body);
  let post = new Post({
    texto: req.body.texto,
    likes: req.body.likes,
    uid: req.body.uid
  });
  let promise = Post.create(post);
  promise.then(
    function(post){
      res.status(201).json(post);
    },
    function(erro){
      res.status(500).json(erro)
    }
  );
};

//EDITAR UM POST
module.exports.editarPost = function(req, res){
  // var id = req.params.id;
  // var postSelecionado = posts.find(function(post){
  //                                         return post._id == id;
  //                                       })
  // if(postSelecionado){
  //   for (var i = 0; i < posts.length; i++) {
  //     if (postSelecionado==posts[i]) {
  //       posts[i] = req.body;
  //       res.status(200).send(req.body);
  //     }
  //   }
  // }else{
  //   res.status(404).send("post não encontrado")
  // }
  let post = new Post({
    texto: req.body.texto,
    likes: req.body.likes,
    uid: req.body.uid
  });

  let tokenQueVem = req.query.token;

  if(tokenQueVem){
    var decoded = jwt.decode(tokenQueVem);
    let id = req.params.id;
    let idUsuarioPost = req.body.uid;
    let idToken = decoded.user._id;
    if(idToken==idUsuarioPost){
      let promise = Post.update(Post.findById(id), req.body);
      promise.then(
        function(post){
          console.log("entro");
          res.status(200).json(post);
        },
        function(erro){
          res.status(500).json(erro);
        }
      )
    }else{
         res.status(401).send("acesso não foi autorizado");
    }
  }else{
    res.status(401).send("acesso não autorizado");
  }

};

//REMOVER UM POST
module.exports.removerPost = function(req, res){
  // var id = req.params.id;
  // var postSelecionado = posts.find(function(post){
  //                                         return post._id == id;
  //                                       })
  // if (postSelecionado) {
  //   for (var i = 0; i < posts.length; i++) {
  //     if (postSelecionado==posts[i]) {
  //       posts.splice(i,1);
  //       res.status(200).send(req.body);
  //     }
  //   }
  // }else{
  //   res.status(404).send("Post não encontrado")
  // }
  let tokenQueVem = req.query.token;

  if(tokenQueVem){
    let decoded = jwt.decode(tokenQueVem);
    let id = req.params.id;
    let idToken = decoded.user._id;
    let uid;
    let postParaExcluir = Post.findById(id).exec();
    postParaExcluir.then(
      function(post){
        uid = post.uid;
        console.log(post);
        excluirPost();
      },
      function(erro){
        res.status(500).json(erro)        
      }
    )
    // let idUsuarioPost = req.body.uid;
    console.log(idToken);
    // console.log(idUsuarioPost);
    function excluirPost(){
      if(idToken==uid){
        let promise = Post.findByIdAndRemove(id);
        promise.then(
          function(post){
            res.status(200).json(post);
          },
          function(erro){
            res.status(500).json(erro);
          }
        )
      }else{
        res.status(401).send("acesso não foi autorizado");
      }
    }
  }else{
      res.status(401).send("acesso não autorizado");
  }  
}

//OBTER USUARIO DE POSTS
module.exports.obterUsuarioDePost = function(req, res){
  // var idPost = req.params.id;
  // var usuarioPost = [];
  // for (var i = 0; i < usuarios.usuarios.length; i++) {
  //   if (usuarios.usuarios[i]._id == idPost) {
  //     usuarioPost.push(usuarios.usuarios[i])
  //   }
  // }
  // if (usuarioPost.length==0) {
  //   res.status(404).send("Não foi postado por ninguem")
  // }
  // res.json(usuarioPost);
  // var idPost = req.params._id;
  // console.log(idPost.uid);
  let post = Post.findById(req.params.id).exec();
  let id;
  post.then(
    function(post){
      console.log(post);
      id = post.uid;
      promiseTeste();
    },
    function(erro){
      console.log(erro);
    }
  )
  function promiseTeste(){
    let promise = Usuario.find({'_id':id}).exec();
    
    // let promise = Post.find()
    promise.then(
      function(usuario){
        // console.log(post);
          res.json(usuario);
      },
      function(erro){
          res.status(404).json(erro);
      }
    )
  }

}
