var controller = require("../controllers/usuarios.js");
var auth = require("../controllers/auth.js");

module.exports = function(app){
  app.post("/api/usuarios/singin", auth.logar);
  app.post("/api/usuarios", controller.inserirUsuario);
  app.use("/api/usuarios", auth.checar);
  app.get("/api/usuarios", controller.listaUsuarios);
  app.get("/api/usuarios/:id", controller.obterUsuario);
  app.put("/api/usuarios/:id", controller.editarUsuario);
  app.delete("/api/usuarios/:id", controller.removerUsuario);
  app.get("/api/usuarios/:id/posts", controller.obterPostsDeUsuario);
}
