//importando o módulo express
var express =  require('express');
var usuariosRouter = require('../app/routes/usuarios.js');
var postsRouter = require('../app/routes/posts.js');
var bodyParser = require('body-parser');
const path = require('path');

//exportando o módulo
module.exports = function(){
  var app = express();
  //definindo variável de aplicação
  app.set("port", 3000);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extend: false }));
  //serve todos os arquivos em public
  app.use(express.static('./public'));
  //adiciona rotas da api
  usuariosRouter(app);
  postsRouter(app);

  app.get('/api/*', (req, res) => {
    res.status(500).send("No endpoint");
  });

  //tudo que sobra redirecionamos para o index.html
  //ex.: localhost:3000/show nao vai cair em nenhum anterior
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname,'../public/index.html'));
  })
  return app;
}
