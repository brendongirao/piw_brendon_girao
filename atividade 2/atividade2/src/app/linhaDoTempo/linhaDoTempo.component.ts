import { Component } from '@angular/core';

import { Post } from './../post/post';
import { PostService } from './../post/post.service';

@Component({
  selector: 'linha-do-tempo',
  templateUrl: './linhaDoTempo.component.html',
})

export class LinhaDoTempo{
  posts:Post[] = null;

  constructor(private postService: PostService){
    this.postService.getPosts().subscribe(
        data => {this.posts = data},
        error => console.log(error)
    );
   }


}
