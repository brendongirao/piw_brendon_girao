import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { NavbarComponent } from './navbar/navbar.component';
import { PostComponent } from './post/post.component';
import { LinhaDoTempo } from './linhaDoTempo/linhaDoTempo.component';
import { PostService} from './post/post.service';
import { PostInputComponent } from './post-input/post-input.component';
import { routing } from './app.routing';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PostComponent,
    LinhaDoTempo,
    PostInputComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
