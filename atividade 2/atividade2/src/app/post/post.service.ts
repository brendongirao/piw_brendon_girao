import { Injectable, Input, Output, EventEmitter} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import { Post } from './post';
import { LinhaDoTempo } from './../linhaDoTempo/linhaDoTempo.component';
import { PostInputComponent } from './../post-input/post-input.component';

@Injectable()
export class PostService{
  static urlPosts = "http://rest.learncode.academy/api/piw/brendon";
  public posts: Post[] = [ ];
  constructor(private http: Http){ }

  inserirPost(novoPost: Post){
    console.log(novoPost);
    this.posts.unshift(novoPost);
    return this.http.post(PostService.urlPosts, novoPost)
        .map((response: Response) =>{ response.json()})
        .catch((error: Response) => Observable.throw(error));
    // console.log(response.id);
  }

  excluirPost(post){
    this.posts.splice(this.posts.indexOf(post), 1);
    return this.http.delete(PostService.urlPosts+"/"+post.id)
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error));
  }

  getPosts(){
    // alert(this.posts.length);
    return this.http.get(PostService.urlPosts)
          .map((response: Response)=>{
            let posts:Post[] = [];
            for(let post of response.json()){
              posts.push(new Post(post.nomePessoa, post.texto, post.id, post.qtdLikes))
            }
            this.posts = posts;
            return posts;
          })
          .catch((error: Response)=> Observable.throw(error));
  }
  atualizarPost(post, novoTexto){
    console.log(novoTexto);
    post.texto=novoTexto;
    console.log(post);
    return this.http.put(PostService.urlPosts+"/"+post.id, post)
  }

  atualizarLikes(post){
    post.qtdLikes++;
    return this.http.put(PostService.urlPosts+"/"+post.id, post)
  }

}
