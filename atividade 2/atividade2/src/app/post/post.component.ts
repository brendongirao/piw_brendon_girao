import { Component, Input, Output, EventEmitter} from '@angular/core';

import { Post } from './post';

import { PostService } from './post.service';

@Component({
  selector:'post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent{

  constructor(public postService: PostService){  }

  classes={'editar-aberto':false,
            'editar-fechado':true};
  classesTextoAtual={
    'editar-aberto':true,
    'editar-fechado':false
  }
  editou(e){
    this.classes={
      'editar-aberto':true,
      'editar-fechado':false
    }
    this.classesTextoAtual={
      'editar-aberto':false,
      'editar-fechado':true
    }
  }
  cancelou(e){
    this.classes={
      'editar-aberto':false,
      'editar-fechado':true
    }
    this.classesTextoAtual={
      'editar-aberto':true,
      'editar-fechado':false
    }
  }
  salvou(post, novoTexto){
    this.postService.atualizarPost(post, novoTexto).subscribe(data=>{console.log(data)},error=>console.log(error));
    this.classes={
      'editar-aberto':false,
      'editar-fechado':true
    }
    this.classesTextoAtual={
      'editar-aberto':true,
      'editar-fechado':false
    }
  }

  @Input() post:Post;
  // @Output() foiClicado = new EventEmitter<number>();

  recebeuLike(post) {
    console.log(post);
    this.postService.atualizarLikes(post).subscribe(data=>{console.log(data)},error=>console.log(error));
    // this.foiClicado.emit(this.post.idPost);
    // console.log("id do post: " + this.post.idPost);
  }
  excluiu(post){
    this.postService.excluirPost(post).subscribe(data=>{console.log(data)},error=>console.log(error));
  }
}
