import { Routes, RouterModule } from "@angular/router";

import { PostInputComponent } from './post-input/post-input.component';

import { LinhaDoTempo } from './linhaDoTempo/linhaDoTempo.component';

const APP_ROUTES: Routes = [
  {path: "", redirectTo: "/linha-do-tempo/show", pathMatch:"full"},
  {path: "linha-do-tempo/show", component: LinhaDoTempo},
  {path: "postar/create", component: PostInputComponent},
]

export const routing = RouterModule.forRoot(APP_ROUTES);
