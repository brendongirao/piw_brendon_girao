import { Component, OnInit } from '@angular/core';

import { Post } from './../post/post';
import { PostService } from './../post/post.service';
import { PostComponent } from './../post/post.component';


@Component({
  selector: 'app-post-input',
  templateUrl: './post-input.component.html',
  styleUrls: ['./post-input.component.css']
})
export class PostInputComponent implements OnInit {

  // static id:number = 0;
  constructor(public postService: PostService) { }

  nome:string = "";
  texto:string ="";
  id:number=0;
  qntdLikes:number=0;

  // static ids = [];

  submeter(nome, texto){
  console.log(nome);
  console.log(texto);
  this.postService.inserirPost(new Post(this.nome, this.texto, this.id, this.qntdLikes))
                  .subscribe(data=>console.log(this.nome),error=>console.log(error));
  }

  ngOnInit() {
  }

}
